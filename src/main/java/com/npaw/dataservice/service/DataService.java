package com.npaw.dataservice.service;

import javax.validation.constraints.NotNull;

import com.npaw.dataservice.model.RequestFilter;
import com.npaw.dataservice.model.ResponseData;

public interface DataService {

    ResponseData getData(@NotNull final RequestFilter requestFilter);

}
