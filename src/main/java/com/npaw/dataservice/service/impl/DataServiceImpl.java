package com.npaw.dataservice.service.impl;

import com.npaw.dataservice.model.*;
import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.entity.Host;
import com.npaw.dataservice.service.DataService;
import com.npaw.dataservice.repository.DataRepository;
import org.apache.commons.math3.distribution.EnumeratedDistribution;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class DataServiceImpl implements DataService {

    private static final String UNIQUE_CODE_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final String DEFAULT_HOST = "default-host";
    private final SecureRandom secureRandom = new SecureRandom();
    private static final int UNIQUE_CODE_LENGTH = 16;

    @Autowired
    private DataRepository dataRepository;

    @Override
    public synchronized ResponseData getData(@NotNull final RequestFilter requestFilter) {
        final Configuration configuration = dataRepository.findDeviceByFilter(requestFilter);
        if (configuration == null) {
            return null;
        }
        String host = getLoadBalancedHost(configuration.getHosts());
        String uniqueCode = getUniqueCode();

        return new ResponseData(host, configuration.getPingTime(), uniqueCode);
    }

    private String getLoadBalancedHost(final Set<Host> hosts) {
        if (!hosts.isEmpty()) {
            List<Pair<String, Double>> hostPairList = new ArrayList<>();
            hosts.forEach(h -> hostPairList.add(new Pair<String, Double>(h.getUrl(), h.getLoad())));
            EnumeratedDistribution<String> enumeratedDistribution = new EnumeratedDistribution<>(hostPairList);
            return enumeratedDistribution.sample();
        }
        return DEFAULT_HOST;
    }

    private String getUniqueCode() {
        final StringBuilder sb = new StringBuilder(UNIQUE_CODE_LENGTH);
        for(int i = 0; i < UNIQUE_CODE_LENGTH; i++ ) {
            sb.append(UNIQUE_CODE_CHARS.charAt(secureRandom.nextInt(UNIQUE_CODE_CHARS.length())));
        }
        return sb.toString();
   }
}
