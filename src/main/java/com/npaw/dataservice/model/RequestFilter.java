package com.npaw.dataservice.model;

public class RequestFilter {

    private String accountCode;
    private String targetDevice;
    private String pluginVersion;

    public RequestFilter(String accountCode, String targetDevice, String pluginVersion) {
        this.accountCode = accountCode;
        this.targetDevice = targetDevice;
        this.pluginVersion = pluginVersion;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public String getTargetDevice() {
        return targetDevice;
    }

    public String getPluginVersion() {
        return pluginVersion;
    }
}
