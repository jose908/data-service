package com.npaw.dataservice.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "q")
public class ResponseData {

    private String cluster;
    private long pingTime;
    private String code;

    public ResponseData(String cluster, long pingTime, String code) {
        this.cluster = cluster;
        this.pingTime = pingTime;
        this.code = code;
    }

    @JacksonXmlProperty(localName = "h")
    public String getCluster() {
        return cluster;
    }

    @JacksonXmlProperty(localName = "pt")
    public long getPingTime() {
        return pingTime;
    }

    @JacksonXmlProperty(localName = "c")
    public String getCode() {
        return code;
    }
}
