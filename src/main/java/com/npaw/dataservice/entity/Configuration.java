package com.npaw.dataservice.entity;

import java.util.Objects;
import java.util.Set;

public class Configuration {

    private String targetDevice;
    private String pluginVersion;
    private int pingTime;
    private Set<Host> hosts;

    public String getTargetDevice() {
        return targetDevice;
    }

    public String getPluginVersion() {
        return pluginVersion;
    }

    public int getPingTime() {
        return pingTime;
    }

    public Set<Host> getHosts() {
        return hosts;
    }

    public void setTargetDevice(String targetDevice) {
        this.targetDevice = targetDevice;
    }

    public void setPluginVersion(String pluginVersion) {
        this.pluginVersion = pluginVersion;
    }

    public void setPingTime(int pingTime) {
        this.pingTime = pingTime;
    }

    public void setHosts(Set<Host> hosts) {
        this.hosts = hosts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Configuration configuration = (Configuration) o;
        return pingTime == configuration.pingTime &&
                Objects.equals(targetDevice, configuration.targetDevice) &&
                Objects.equals(pluginVersion, configuration.pluginVersion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(targetDevice, pluginVersion);
    }
}
