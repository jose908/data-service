package com.npaw.dataservice.entity;

import java.util.Objects;

public class Host {

    private String url;
    private Double load;

    public String getUrl() {
        return url;
    }

    public Double getLoad() {
        return load;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setLoad(Double load) {
        this.load = load;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Host host = (Host) o;
        return Objects.equals(url, host.url) &&
                Objects.equals(load, host.load);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, load);
    }
}
