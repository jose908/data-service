package com.npaw.dataservice.entity;

import java.util.Objects;
import java.util.Set;

public class Account {

    private String code;
    private Set<Configuration> configurations;

    public String getCode() {
        return code;
    }

    public Set<Configuration> getConfigurations() {
        return configurations;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setConfigurations(Set<Configuration> configurations) {
        this.configurations = configurations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Account account = (Account) o;
        return Objects.equals(code, account.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
