package com.npaw.dataservice.repository;

import com.npaw.dataservice.entity.Account;
import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.model.RequestFilter;

import java.util.Set;

public interface DataRepository {

    Configuration findDeviceByFilter(final RequestFilter requestFilter);

    Set<Account> getAllAccounts();

    boolean updateAccount(Account account);

    boolean createAccount(Account account);

    boolean deleteAccount(Account account);

}
