package com.npaw.dataservice.repository.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.npaw.dataservice.entity.Account;
import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.model.RequestFilter;
import com.npaw.dataservice.repository.DataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
public class DataRepositoryImpl implements DataRepository {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private Set<Account> accounts;

    @Value("${npaw.intial.data}")
    private String filePath;

    @PostConstruct
    public void initClientData() {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            File file = new File(filePath);
            accounts = jsonMapper.readValue(file,  new TypeReference<Set<Account>>(){});
            LOG.info("File read correctly");
        } catch (IOException e) {
            LOG.error("File could not be read or found {}", e);
            accounts = new HashSet<>();
        }
    }

    @Override
    public Configuration findDeviceByFilter(@NotNull final RequestFilter requestFilter) {
       return accounts.stream()
               .filter(a -> requestFilter.getAccountCode().equals(a.getCode()))
               .flatMap(a -> a.getConfigurations().stream())
               .filter(c -> requestFilter.getTargetDevice().equals(c.getTargetDevice()) && requestFilter.getPluginVersion().equals(c.getPluginVersion()))
               .findFirst().orElse(null);
    }

    @Override
    public Set<Account> getAllAccounts() {
        return accounts;
    }

    @Override
    public boolean updateAccount(Account account) {
        return accounts.remove(account) && accounts.add(account);
    }

    @Override
    public boolean createAccount(Account account) {
        return accounts.add(account);
    }

    @Override
    public boolean deleteAccount(Account account) {
        return accounts.remove(account);
    }
}
