package com.npaw.dataservice.controller;

import java.util.Collections;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.npaw.dataservice.entity.Account;
import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.repository.DataRepository;

@RestController()
public class ApiController {

    @Autowired
    private DataRepository dataRepository;

    @RequestMapping(value = "/accounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Account> getAccounts() {
        return dataRepository.getAllAccounts();
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Account createAccount(@RequestBody final Account account, final HttpServletResponse response) {
        boolean isAccountCreated = dataRepository.createAccount(account);
        if (isAccountCreated) {
            response.setStatus(HttpServletResponse.SC_CREATED);
            return account;
        }
        return null;
    }

    @RequestMapping(value = "/accounts/{accountCode}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Configuration>  updateAccountConfiguration(@PathVariable final String accountCode, @RequestBody final Set<Configuration> configurationSet,
            final HttpServletResponse response) {
        Account account = new Account();
        account.setCode(accountCode);
        account.setConfigurations(configurationSet);
        boolean isAccountUpdated = dataRepository.updateAccount(account);
        if (isAccountUpdated) {
            return configurationSet;
        }
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        return Collections.emptySet();
    }

    @RequestMapping(value = "/accounts/{accountCode}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAccount(@PathVariable final String accountCode, HttpServletResponse response) {
        Account account = new Account();
        account.setCode(accountCode);
        boolean isAccountDeleted = dataRepository.deleteAccount(account);
        if (!isAccountDeleted) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
