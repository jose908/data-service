package com.npaw.dataservice.controller;

import com.npaw.dataservice.model.ResponseData;
import com.npaw.dataservice.model.RequestFilter;
import com.npaw.dataservice.service.DataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class DataController {

    @Autowired
    private DataService dataRetrieverService;

    @RequestMapping(value = "/getData", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseData getData(@RequestParam() String accountCode,
                                @RequestParam String targetDevice, @RequestParam String pluginVersion) {
        RequestFilter requestFilter = new RequestFilter(accountCode, targetDevice, pluginVersion);
        return this.dataRetrieverService.getData(requestFilter);
    }

}
