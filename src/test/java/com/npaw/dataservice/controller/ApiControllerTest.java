package com.npaw.dataservice.controller;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.npaw.dataservice.repository.DataRepository;
import com.npaw.dataservice.service.DataService;

@RunWith(SpringRunner.class)
@WebMvcTest(ApiController.class)
public class ApiControllerTest {

    @MockBean
    private DataRepository dataRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllAccountsShouldReturnAllAccounts() throws Exception {
        when(dataRepository.getAllAccounts()).thenReturn(new HashSet<>());
        this.mockMvc.perform(get("/accounts")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }
    @Test
    public void updateExistingAccountShouldReturnOK() throws Exception {
        when(dataRepository.updateAccount(anyObject())).thenReturn(true);
        this.mockMvc.perform(put("/accounts/clienteA")
                .content("[ { \"targetDevice\": \"osmf\", \"pluginVersion\": \"3.3.1\", \"pingTime\": 5, \"hosts\": [ { \"url\": \"clusterB.com\", \"load\": 50 }, { \"url\": \"clusterA.com\", \"load\": 50 } ] } ]")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
    @Test
    public void updateNotExistingAccountShouldReturnNotFound() throws Exception {
        when(dataRepository.updateAccount(anyObject())).thenReturn(false);
        this.mockMvc.perform(put("/accounts/clienteA")
                .content("[ { \"targetDevice\": \"osmf\", \"pluginVersion\": \"3.3.1\", \"pingTime\": 5, \"hosts\": [ { \"url\": \"clusterB.com\", \"load\": 50 }, { \"url\": \"clusterA.com\", \"load\": 50 } ] } ]")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }
    @Test
    public void badFormedUpdateAccountJsonRequestShouldReturnBadRequest() throws Exception {
        when(dataRepository.updateAccount(anyObject())).thenReturn(true);
        this.mockMvc.perform(put("/accounts/clienteA")
                .content("badRequest")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void createNewValidAccountShouldReturnCreated() throws Exception {
        when(dataRepository.createAccount(anyObject())).thenReturn(true);
        this.mockMvc.perform(post("/accounts")
                .content("{ \"code\": \"clienteB\", \"configurations\": [ { \"targetDevice\": \"osmf\", \"pluginVersion\": \"3.3.1\", \"pingTime\": 5, \"hosts\": [ { \"url\": \"clusterB.com\", \"load\": 50 }, { \"url\": \"clusterA.com\", \"load\": 50 } ] } ] }")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }
    @Test
    public void createNotValidAccountShouldReturnOkNotEmpty() throws Exception {
        when(dataRepository.createAccount(anyObject())).thenReturn(false);
        this.mockMvc.perform(post("/accounts")
                .content("{ \"code\": \"clienteB\", \"configurations\": [ { \"targetDevice\": \"osmf\", \"pluginVersion\": \"3.3.1\", \"pingTime\": 5, \"hosts\": [ { \"url\": \"clusterB.com\", \"load\": 50 }, { \"url\": \"clusterA.com\", \"load\": 50 } ] } ] }")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
    @Test
    public void badFormedCreateAccountJsonRequestShouldReturnBadRequest() throws Exception {
        when(dataRepository.createAccount(anyObject())).thenReturn(false);
        this.mockMvc.perform(post("/accounts")
                .content("badRequest")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void deleteExistingAccountShouldReturnOK() throws Exception {
        when(dataRepository.deleteAccount(anyObject())).thenReturn(true);
        this.mockMvc.perform(delete("/accounts/clienteA"))
                .andExpect(status().isOk());
    }
    @Test
    public void deleteNonExistingAccountShouldReturnNOtFound() throws Exception {
        when(dataRepository.deleteAccount(anyObject())).thenReturn(false);
        this.mockMvc.perform(delete("/accounts/clienteA"))
                .andExpect(status().isNotFound());
    }



}