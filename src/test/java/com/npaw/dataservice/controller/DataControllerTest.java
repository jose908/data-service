package com.npaw.dataservice.controller;

import com.npaw.dataservice.model.ResponseData;
import com.npaw.dataservice.service.DataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(DataController.class)
public class DataControllerTest {

    @MockBean
    private DataService dataService;

    @Autowired
    private MockMvc mockMvc;

    private ResponseData responseData = new ResponseData("test",10, "test");

    @Test
    public void getDataWithAllParamsShouldReturnOk() throws Exception {
        when(this.dataService.getData(anyObject())).thenReturn(responseData);
        this.mockMvc.perform(get("/getData?accountCode=clienteA&targetDevice=XBox&pluginVersion=3.3.1")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(content().string("<q><h>test</h><pt>10</pt><c>test</c></q>"));
    }
    @Test
    public void getDataWithNotAllParamsShouldReturnNotFound() throws Exception {
        this.mockMvc.perform(get("/getData?accountCode=clienteA&targetDevice=XBox")
                .accept(MediaType.ALL))
                .andExpect(status().isBadRequest());
    }
    @Test
    public void getDataWithAllParamsButNoDataShouldReturnOkAndEmpty() throws Exception {
        when(this.dataService.getData(anyObject())).thenReturn(null);
        this.mockMvc.perform(get("/getData?accountCode=clienteA&targetDevice=XBox&pluginVersion=3.3.1")
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(content().string(""));
    }
    @Test
    public void getDataWithValidParamsShouldReturnAnObject() {
        when(this.dataService.getData(anyObject())).thenReturn(responseData);

    }
}
