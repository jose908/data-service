package com.npaw.dataservice.entity;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class ConfigurationTest {

    @Test
    public void samePluginAndDeviceConfigurationObjectShouldBeEqual() throws Exception {
        Configuration configuration1 = new Configuration();
        configuration1.setPluginVersion("3.3.1");
        configuration1.setTargetDevice("device");
        Configuration configuration2 = new Configuration();
        configuration2.setPluginVersion("3.3.1");
        configuration2.setTargetDevice("device");
        Assert.assertTrue(configuration1.equals(configuration2));
    }
    @Test
    public void differentPluginAndDeviceConfigurationObjectShouldBeNotEqual() throws Exception {
        Configuration configuration1 = new Configuration();
        configuration1.setPluginVersion("3.3.1");
        configuration1.setTargetDevice("device1");
        Configuration configuration2 = new Configuration();
        configuration2.setPluginVersion("3.3.1");
        configuration2.setTargetDevice("device2");
        Assert.assertFalse(configuration1.equals(configuration2));
    }

}