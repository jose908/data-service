package com.npaw.dataservice.entity;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

public class AccountTest {

    @Test
    public void sameCodeAccountObjectShouldBeEqual() throws Exception {
        Account account1 = new Account();
        account1.setCode("myCode");
        Account account2 = new Account();
        account2.setCode("myCode");
        Assert.assertTrue(account1.equals(account2));
    }
    @Test
    public void differentCodeAccountObjectsShouldNotBeEqual() throws Exception {
        Account account1 = new Account();
        account1.setCode("myCode");
        Account account2 = new Account();
        account2.setCode("myCode2");
        Assert.assertFalse(account1.equals(account2));
    }

}