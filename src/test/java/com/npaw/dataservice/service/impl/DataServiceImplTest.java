package com.npaw.dataservice.service.impl;

import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.entity.Host;
import com.npaw.dataservice.model.RequestFilter;
import com.npaw.dataservice.model.ResponseData;
import com.npaw.dataservice.repository.DataRepository;
import com.npaw.dataservice.service.DataService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataServiceImplTest {

    @Mock
    private DataRepository dataRepository;

    @InjectMocks
    private DataService dataService = new DataServiceImpl();

    private RequestFilter requestFilter = new RequestFilter("test", "test" , "10");

    private Configuration configuration = new Configuration();

    @Before
    public void setUp(){
        configuration.setTargetDevice("test");
        configuration.setPluginVersion("test");
        configuration.setPingTime(1);
        Set<Host> hostSet = new HashSet<>();
        Host host = new Host();
        host.setLoad(1d);
        host.setUrl("host");
        hostSet.add(host);
        configuration.setHosts(hostSet);
    }

    @Test
    public void notFoundDataShouldReturnNull() {
        when(dataRepository.findDeviceByFilter(anyObject())).thenReturn(null);
        Assert.assertNull(dataService.getData(requestFilter));
    }

    @Test
    public void uniqueCodeShouldBeUniqueInEachCall() {
        when(dataRepository.findDeviceByFilter(anyObject())).thenReturn(configuration);
        ResponseData responseData1 = dataService.getData(requestFilter);
        ResponseData responseData2 = dataService.getData(requestFilter);
        Assert.assertNotEquals(responseData1.getCode(),responseData2.getCode());
    }

    @Test
    public void foundDataShouldReturnCorrectResponseData() {
        when(dataRepository.findDeviceByFilter(anyObject())).thenReturn(configuration);
        ResponseData responseData = dataService.getData(requestFilter);
        Assert.assertEquals("host", responseData.getCluster());
        Assert.assertEquals(1, responseData.getPingTime());
    }







}