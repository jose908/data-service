package com.npaw.dataservice.repository.impl;

import com.npaw.dataservice.entity.Account;
import com.npaw.dataservice.entity.Configuration;
import com.npaw.dataservice.model.RequestFilter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class DataRepositoryImplTest {

    private DataRepositoryImpl dataRepository = new DataRepositoryImpl();

    @Before
    public void setUpFile() {
        ReflectionTestUtils.setField(dataRepository, "filePath", "initial-data.json");
        dataRepository.initClientData();
    }

    @Test
    public void getAllAccountsMustReturnAllAccounts() {
        Assert.assertEquals(2, dataRepository.getAllAccounts().size());
    }

    @Test
    public void getValidConfigurationByFilterShouldReturnAValidObject() {
        RequestFilter requestFilter = new RequestFilter("clienteA", "XBox", "3.3.1");
        Configuration configuration = dataRepository.findDeviceByFilter(requestFilter);
        Assert.assertEquals("XBox", configuration.getTargetDevice());
    }
    @Test
    public void getNonValidConfigurationByFilterShouldReturnNull() {
        RequestFilter requestFilter = new RequestFilter("clienteD", "XBox", "3.3.1");
        Configuration configuration = dataRepository.findDeviceByFilter(requestFilter);
        Assert.assertNull(configuration);
    }

    @Test
    public void updateExistingAccountShouldReturnTrue() {
        Account account = new Account();
        account.setCode("clienteA");
        Assert.assertTrue(dataRepository.updateAccount(account));
    }

    @Test
    public void updateNonExistingAccountShouldReturnFalse() {
        Account account = new Account();
        account.setCode("clienteC");
        Assert.assertFalse(dataRepository.updateAccount(account));
    }

    @Test
    public void createNonExistingAccountShouldReturnTrue() {
        Account account = new Account();
        account.setCode("clienteC");
        Assert.assertTrue(dataRepository.createAccount(account));
    }

    @Test
    public void createExistingAccountShouldReturnFalse() {
        Account account = new Account();
        account.setCode("clienteA");
        Assert.assertFalse(dataRepository.createAccount(account));
    }

    @Test
    public void deleteNonExistingAccoundShouldReturnFalse() {
        Account account = new Account();
        account.setCode("clienteD");
        Assert.assertFalse(dataRepository.deleteAccount(account));
    }

    @Test
    public void deleteExistingAccoundShouldReturnTrue() {
        Account account = new Account();
        account.setCode("clienteA");
        Assert.assertTrue(dataRepository.deleteAccount(account));
    }



}