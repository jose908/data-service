# NPAW JAVA TECH TEST
Simple Java rest service built with Spring Boot as part of the hiring process. An executable fat Jar is provided with the delivery.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
### Prerequisites

In order to build and run the project you'll need:

```
Jdk8
Maven
Internet connection for downloading the dependencies
```

### Build from source

For building the project and executing the tests:

```sh
$ cd data-service
$ mvn clean install
```
### Run from source

For running the server project from the comand line (will start on port 80):

```sh
$ cd data-service
$ mvn spring-boot:run
```
### Package Jar and run

For creating a fat will all dependencies and then running the server (will start on port 80):

```sh
$ cd data-service
$ mvn clean package
$ java -jar /target/data-service-1.0.0.jar
```
Important, `inital-data.json` should be placed in the same `Jar` directory for providing the initial demo data.

# Crud API
Apart from the required `getData` endpoint, a restful api is provided to change the initial `inital-data.json` while the server is running:

### Get all the accounts

Gets all the accounts stored in the the server:

````
GET /accounts
````
#### Response
`````JSON
Status: 200 OK
[
    {
        "code": "clienteB",
        "configurations": [
            {
                "targetDevice": "osmf",
                "pluginVersion": "3.3.1",
                "pingTime": 5,
                "hosts": [
                    {
                        "url": "clusterB.com",
                        "load": 50
                    },
                    {
                        "url": "clusterA.com",
                        "load": 50
                    }
                ]
            }
        ]
    }
]
`````

### Create an account

Creates a new account;
````
POST /accounts
````
#### Request Body
The following example object has to be provided:
`````JSON
{
   "code":"clienteB",
   "configurations":[
      {
         "targetDevice":"osmf",
         "pluginVersion":"3.3.1",
         "pingTime":5,
         "hosts":[
            {
               "url":"clusterB.com",
               "load":50
            },
            {
               "url":"clusterA.com",
               "load":50
            }
         ]
      }
   ]
}
`````
If the account is updated a `201` HTTP code will be received.

### Update an account
Updates the configuration of an existing account;
````
PUT /accounts/:accountName
````
#### Request Body
The following example object has to be provided:
`````JSON
[
   {
      "targetDevice":"osmf",
      "pluginVersion":"3.3.1",
      "pingTime":5,
      "hosts":[
         {
            "url":"clusterB.com",
            "load":50
         },
         {
            "url":"clusterA.com",
            "load":50
         }
      ]
   }
]
`````
If the account is created a `200` HTTP code will be received.
### Delete an account
Deletes the  existing account
````
DELETE /accounts/:accountName
````

If the account is deleted a `200` HTTP code will be received.
## Built With

* [Spring Boot](https://projects.spring.io/spring-boot/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Jackson](https://github.com/FasterXML/jackson) - For XML and JSON parsing
* [commons-math3](http://commons.apache.org/proper/commons-math/) - For stadistical calculations